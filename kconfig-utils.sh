#!/bin/bash

<<COPYLEFT

KConfig utils

Copyright (C) 2023-2024  Gioacchino Mazzurco <gio@eigenlab.org>

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>

SPDX-License-Identifier: AGPL-3.0-only

COPYLEFT

## Implementation private stuff is prefixed with 'kconfig:'.
## API function and variables (intented for use by others) are prefixed
## with 'kconfig_' and 'KCONFIG_'

## Print debug message to stderr with reasonable "decoration"
function kconfig:dbg()
{
	>&2 echo $(basename "${BASH_SOURCE}") $@;
}

## Define default value for variable, take two arguments, $1 variable name,
## $2 default variable value, if the variable is not already define define it
## with default value.
function kconfig:define_default_value()
{
	VAR_NAME="${1}"
	DEFAULT_VALUE="${2}"

	[ -z "${!VAR_NAME}" ] && export ${VAR_NAME}="${DEFAULT_VALUE}" || true
}

## Define this variable if you want to change the location of the `.config` file
## you want to edit
kconfig:define_default_value KCONFIG_CONFIG_PATH ".config"

kconfig:register_path()
{
	echo "/tmp/${KCONFIG_CONFIG_PATH////_}_register"
}

function kconfig_init_register()
{
	echo > "$(kconfig:register_path)"
	kconfig:dbg "Initialized empty register at $(kconfig:register_path)"
}

function kconfig_wipe_register()
{
	kconfig:dbg "Wiping $(kconfig:register_path)"
	rm "$(kconfig:register_path)"
}

function kconfig:assign_conf()
{
	local mConfigFullName="$1"
	local mConfigFullLine="${2//\"/\\\"}" # escape "

	function mAwkScript()
	{
		cat <<EOF
BEGIN { mFoud=0 }
{
	mFoud+=sub(/^.*$mConfigFullName[ =].*$/, "${mConfigFullLine}");
	print;
}
END { if(!mFoud) print "$mConfigFullLine"; }
EOF
	}

	mAwkScript | gawk --file=- --include inplace "$KCONFIG_CONFIG_PATH"
	mAwkScript | gawk --file=- --include inplace "$(kconfig:register_path)"
}

function kconfig_clear()
{
	local mConfigFullName="$1"

	local mConfs=$(<"$KCONFIG_CONFIG_PATH")
	echo "${mConfs}" | \
		grep --fixed-strings --word-regexp --invert-match ${mConfigFullName} > \
			"$KCONFIG_CONFIG_PATH"

	local mConfsRegister=$(<"$(kconfig:register_path)")
	echo "${mConfsRegister}" | \
		grep --fixed-strings --word-regexp --invert-match ${mConfigFullName} > \
			"$(kconfig:register_path)"
}

function kconfig_set()
{
	local mConfigFullName="$1"

	# Optional value, "y" by default
	local mConfigValue="${2-y}"

	kconfig:assign_conf "$mConfigFullName" "$mConfigFullName=$mConfigValue"
}

function kconfig_unset()
{
	local mConfigFullName="$1"

	kconfig:assign_conf "$mConfigFullName" "# $mConfigFullName is not set"
}

function kconfig_check()
{
	local mConfigPath="${1-$KCONFIG_CONFIG_PATH}"

	while read mLine ; do
		if [ "${mLine:0:1}" == "#" ] ; then
			# Check that explicitely unset stuff didn't got enabled
			# '# CONFIG_XXXXXX is not set' must never end as CONFIG_XXXXXX=y
			# while it is acceptable if it is either unset, missing, or
			# 'CONFIG_XXXXXX=m'.

			local mConfName=$(echo ${mLine} | awk '{print $2}')
			local mConfLine=$(echo ${mLine} | grep --word-regexp $mConfName "${mConfigPath}")
			
			[ "$mConfLine" == "$mConfName=y" ] &&
			{
				kconfig:dbg "Fatal: explicitely unset config found enabled in \
$mConfigPath Expected '$mLine' found '$mConfLine'"
				exit -1
			}
		elif [ "$(echo ${mLine} | awk -F= '{print $2}')" == "y" ] ; then
			# Check that explicitely enabled things as 'CONFIG_XXXXXX=y' are
			# present in the same way in .config. Anything else is a fatal
			# error.
			grep --fixed-strings --line-regexp -q "${mLine}" "${mConfigPath}" ||
			{
				kconfig:dbg "Fatal: explicitely enabled config ${mLine} not  \
found enabled in $mConfigPath"
				exit -2
			}
		elif [ "$(echo ${mLine} | awk -F= '{print $2}')" == "m" ] ; then
			# Check that explicitely enabled things as 'CONFIG_XXXXXX=m' are
			# present either as 'm' or 'y' in .config.
			# Absence or unset is a fatal error.
			local mConfName=$(echo ${mLine} | awk -F= '{print $1}')
			grep --fixed-strings --line-regexp -q "${mLine}" "${mConfigPath}" ||
				grep --fixed-strings --line-regexp -q "${mConfName}=y" "${mConfigPath}" ||
			{
				kconfig:dbg "Fatal: explicitely module config ${mLine} not \
found enabled in $mConfigPath"
				exit -3
			}
		fi
	done < "$(kconfig:register_path)"

	kconfig:dbg "check_configs successful $mConfigPath is consistent with \
$(kconfig:register_path)"
}
