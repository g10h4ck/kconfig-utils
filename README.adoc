= Kconfig Utils

Did you remained sadly surprised by the lack of a way to programmatically
configure build options of Linux kernel? I have.

I needed to configure Linux kernel and OpenWrt (which uses Kconfig too) build
options from a script but found no proper way to do it, so I had to come up with
this.

Kconfig utils is a minimal bash library which let you edit programmatically
Linux `.config`, or any other sofware which uses Kconfig for build time
configuration, in a way you can rely on.

Source it in your script, set/unset the options you need, run `make defconfig`,
check the result for consistency, and compile your kernel.

Some will scream horrified
__but you can just copy `.config` and then run `make oldconfig`!__
I wish you good luck keeping track of your personal changes accross versions in
that case.

.Simple OpenWrt usage example
[source,bash]
--------------------------------------------------------------------------------
cd "$HOME/openwrt-build/"

# Update and install OpenWrt feeds as usual
./scripts/feeds update -a
./scripts/feeds install -a

# Tell Kconfig utils were your `.config` is located and and then include it
export KCONFIG_CONFIG_PATH="$(pwd)/.config"
source "$HOME/Development/kconfig-utils/kconfig-utils.sh"

# If you need to you can wipe your own `.config` to start a new
echo "" > "$KCONFIG_CONFIG_PATH"

# Initialize an empty config changes register, needed to check consistentcy
kconfig_init_register

# Set target, needed by OpenWrt to generate default configuration
kconfig_set CONFIG_TARGET_ath79
kconfig_set CONFIG_TARGET_ath79_generic

# Running `make defconfig` here to generate de default configuration is
# necessary, otherwise the added packages configuration will be mangled
# by the next run of `make defconfig` in an unrealiable manner
make defconfig

# Remove a bunch of packages which are enabled by default, but non needed by you
kconfig_unset CONFIG_PACKAGE_ppp
kconfig_unset CONFIG_PACKAGE_ppp-mod-pppoe
kconfig_unset CONFIG_PACKAGE_kmod-ppp
kconfig_unset CONFIG_PACKAGE_kmod-pppoe
kconfig_unset CONFIG_PACKAGE_kmod-pppox

# Set a few options that are not enabled by default, but that you need
kconfig_set CONFIG_PACKAGE_ATH_DEBUG
kconfig_set CONFIG_PACKAGE_ATH_DYNACK
kconfig_set CONFIG_PACKAGE_kmod-usb-ledtrig-usbport

kconfig_set CONFIG_DEVEL # Needed by BUILD_LOG
kconfig_set CONFIG_BUILD_LOG

# run `make defconfig` again to accomodate changes to the `.config` file,
# this will also select needed dependencies and fix option ordering, which
# is important for a proper build,
make defconfig

# Check configuration, if some configuration got mangled by `make defconfig`
# the check will fail, this can happen if some selected option have a missing
# dependency that needs to be selected explicitely too, like `CONFIG_BUILD_LOG`
# here in the example which needs `CONFIG_DEVEL` to be selected previously.
kconfig_check

# At this point changes register can be wiped out
kconfig_wipe_register

# Build your own OpenWrt image as usual
make
--------------------------------------------------------------------------------


.Simple Gentoo linux kernel usage example
[source,bash]
--------------------------------------------------------------------------------
cd /usr/src/linux

echo "" > .config
make defconfig

# Initialize an empty config changes register, needed to check consistentcy
kconfig_init_register

kconfig_set CONFIG_GENTOO_LINUX
kconfig_set CONFIG_GENTOO_LINUX_UDEV
kconfig_set CONFIG_GENTOO_LINUX_PORTAGE
kconfig_set CONFIG_GENTOO_PRINT_FIRMWARE_INFO

# Enable OpenRC support
kconfig_set CONFIG_GENTOO_LINUX_INIT_SCRIPT

# Disable systemd support
kconfig_unset CONFIG_GENTOO_LINUX_INIT_SYSTEMD

kconfig_set CONFIG_DEVTMPFS
kconfig_set CONFIG_DEVTMPFS_MOUNT

# Even if the CPU is more recent the the touchpad keep needing
# Tiger Lake option to work, after a few hours of investigating
# touchpad recognized just as an emulated generic mouse I found this
# @see https://community.frame.work/t/quick-note-on-the-touchpad-linux-kernel-config-for-the-12th-gen-based-laptop/22267
# @see https://blog.dowhile0.org/2022/06/21/how-to-troubleshoot-deferred-probe-issues-in-linux/
kconfig_unset CONFIG_PINCTRL_ALDERLAKE
kconfig_set CONFIG_PINCTRL_TIGERLAKE


# Auto-complete the configuration with
make olddefconfig

# Check configuration, if some configuration got mangled by `make olddefconfig`
# the check will fail, this can happen if some selected option have a missing
# dependency that needs to be selected explicitely too, or if some selected
# configurations are conflicting like CONFIG_PINCTRL_ALDERLAKE and
# CONFIG_PINCTRL_TIGERLAKE in this case.
kconfig_check


# At this point changes register can be wiped out
kconfig_wipe_register

# Build your own linux kernel as usual
make
--------------------------------------------------------------------------------

For more information read the code and the inline comments.
